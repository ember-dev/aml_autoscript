# EmbER universal bootloader script for S802/S805/S812 devices
echo "Generating bootloader script for S802/S805/S812 devices"
if [ -d out/M8 ]; then rm -rf out/M8; fi
mkdir -p out/M8; cd out/M8
(cat << 'EOF') > ember_sdcard_autoscript
defenv
setenv sdcardboot 'echo testing for bootable sdcard...;if mmcinfo; then run boot_from_sdcard;fi;'
setenv boot_from_sdcard 'if fatload mmc 0 ${loadaddr} kernel.img; then run storeargs;echo boot from sdcard...;bootm ${loadaddr};fi;'
setenv bootcmd 'run sdcardboot; run storeboot'
saveenv
run storeargs
run bootcmd
EOF
mkimage -A arm -O linux -T script -C none -d ember_sdcard_autoscript aml_autoscript >/dev/null
rm ember_sdcard_autoscript
echo "Generated script located at '$(pwd)'"
