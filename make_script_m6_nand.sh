# EmbER NAND bootloader script for M6/MX devices
echo "Generating NAND bootloader script's for M6/MX devices"
if [ -d out/M6/NAND ]; then rm -rf out/M6/NAND; fi
mkdir -p out/M6/NAND; cd out/M6/NAND
(cat << EOF) > ember_m6_autoscript
mmcinfo 0
fatload mmc 0 82000000 bootloader.img
nand erase whole
nand rom_protect off
nand rom_write 82000000 0 80000
nand rom_protect on
save
setenv bootcmd "defenv;saveenv;reset"
saveenv
reset
EOF
mkimage -A arm -O linux -T script -C none -d ember_m6_autoscript aml_autoscript >/dev/null
rm ember_m6_autoscript; cp -f aml_autoscript factory_update_param.ubt
echo "Generated script's located at '$(pwd)'"
