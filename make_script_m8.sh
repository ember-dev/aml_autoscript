# EmbER universal bootloader script for S802/S805/S812 devices
echo "Generating bootloader script for S802/S805/S812 devices"
if [ -d out/M8 ]; then rm -rf out/M8; fi
mkdir -p out/M8; cd out/M8
(cat << EOF) > ember_m8_autoscript
if mmcinfo 0 && fatload mmc 0 12000000 bootloader.img; then
	if mmcinfo 1; then mmc erase 1; mmc erase env; else amlnf erase; fi
	if sf probe 2; then sf erase 0 200000; sf write 12000000 0 80000; else store rom_write 12000000 0 80000; save; fi
fi
reset
EOF
mkimage -A arm -O linux -T script -C none -d ember_m8_autoscript aml_autoscript >/dev/null
rm ember_m8_autoscript
echo "Generated script located at '$(pwd)'"
