# EmbER SPI bootloader script for M6/MX devices
echo "Generating SPI bootloader script's for M6/MX devices"
if [ -d out/M6/SPI ]; then rm -rf out/M6/SPI; fi
mkdir -p out/M6/SPI; cd out/M6/SPI
(cat << EOF) > ember_m6_autoscript
mmcinfo 0
fatload mmc 0 82000000 bootloader.img
nand erase whole
sf probe 2
sf erase 0 600000
sf write 82000000 0 80000
setenv bootcmd "defenv;saveenv;reset"
saveenv
reset
EOF
mkimage -A arm -O linux -T script -C none -d ember_m6_autoscript aml_autoscript >/dev/null
rm ember_m6_autoscript; cp -f aml_autoscript factory_update_param.ubt
echo "Generated script's located at '$(pwd)'"
