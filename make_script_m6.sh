# EmbER universal bootloader script for M6/MX devices
echo "Generating bootloader script's for M6/MX devices"
if [ -d out/M6 ]; then rm -rf out/M6; fi
mkdir -p out/M6; cd out/M6
(cat << EOF) > ember_m6_autoscript
if mmcinfo 0 && fatload mmc 0 82000000 bootloader.img; then
	if mmcinfo 1; then mmc erase 1; mmc erase env; else amlnf erase; fi
	if sf probe 2; then sf erase 0 600000; sf write 82000000 0 80000; else store rom_write 82000000 0 80000; save; fi
	setenv bootcmd "defenv;saveenv;reset"; saveenv
fi
reset
EOF
mkimage -A arm -O linux -T script -C none -d ember_m6_autoscript aml_autoscript >/dev/null
rm ember_m6_autoscript; cp -f aml_autoscript factory_update_param.ubt
echo "Generated script's located at '$(pwd)'"
